"use strict";

var gulp, sass, bourbon, neat, fontAwesome, browserSync, minify, 
    js_src, js_dist;

gulp        = require("gulp");
sass        = require("gulp-sass");
bourbon     = require("node-bourbon");
neat        = require("node-neat").includePaths;
fontAwesome = require('node-font-awesome');
browserSync = require("browser-sync");
minify      = require('gulp-minify');

js_src  = './src/javascripts';
js_dist = './js';

console.log(bourbon.with(fontAwesome.scssPath));

// Compile SASS files
gulp.task("sass", function() {
  gulp.src("src/scss/**/*.scss")
      .pipe(sass({
        includePaths: bourbon.with(fontAwesome.scssPath),
        includePaths: neat
      }))
      .pipe(gulp.dest("app/css"))
      .pipe(browserSync.reload({
        stream: true
      }))
});

// Prepare image assets
gulp.task("img", function() {
  gulp.src("src/img/*.*")
      .pipe(gulp.dest("app/img"))
});

// Copy fonts
gulp.task('fonts', function() {
    gulp.src(fontAwesome.fonts)
          .pipe(gulp.dest("app/fonts"))
});

// Prepare HTML files
// gulp.task("html", function() {
//   gulp.src("src/*.html")
//       .pipe(gulp.dest("."))
// });

// Prepare JS files
//gulp.task('jsmin', function () {
//  gulp.src(js_src + '/**/*.js')
//    .pipe(minify({
//        ext: {
//            src:'.js',
//            min:'.min.js'
//        }
//    }))
//    .pipe(gulp.dest(js_dist));
//});

// Spin up a server
gulp.task("browserSync", function() {
  browserSync({
    server: {
      baseDir: "./app"
    }
  })
});

// Live reload anytime a file changes
gulp.task("watch", ["browserSync", "sass", "img"], function() {
    gulp.watch("src/scss/**/*.scss", ["sass"]);
    gulp.watch(fontAwesome.fonts, ["fonts"]);
    gulp.watch("src/img/*.*", ["img"]);
    // gulp.watch(js_src + '/*.js', ["jsmin"]);
    gulp.watch("app/**/*.html").on("change", browserSync.reload);
});

// Compiles all gulp tasks
// gulp.task("default", ["sass", "img", "fonts", "html", "slim", "jsmin"]);
gulp.task("default", ["sass", "img", "fonts"]);
