/*global angular */
/*global document */
/*global ol */
/*jslint node: true */

'use strict';

angular.module('app', ['ngRoute', 'ngSanitize', 'appConfig', 'utils'])

.constant('nodeTypes', {
  CHOICE: "choice",
  INFO: "info"
})

.constant('events', {
  STEP_NEXT: 10,
  STEP_LOADED: 11,
  STEP_FAILED: 12
})

.config(function($routeProvider, $logProvider, config) {

  $routeProvider
    .when('/step/:root/:id/', {
      templateUrl: 'partials/step.html'
    })
    .otherwise({
      redirectTo: '/step/root/root/'
    });

  $logProvider.debugEnabled(config.debug);

})

.factory('context', function($log, $location, $rootScope, $q, $httpParamSerializer, graph, events, nodeTypes) {

  function Context() {
    this.steps = [];
    this.rootId = null;
    this.loading = false;
    this.currentStep = null;
  }

  Context.prototype = {

    get choices() {
      return this.steps.reduce(function(acc, s) {
        return angular.extend(acc, s.choices);
      }, {});
    },
    
    get startIds() {
      return this.steps.reduce(function(acc, s) {
        acc.push(s.startId);
        return acc;
      }, []);
    },

    addStep: function(id, nodes, choices) {
      var s = new Step(id, nodes, choices);
      this.steps.push(s);
      $log.debug("Got new step", this.steps);
      return s;
    },

  };

  var context = new Context();

  function Step(id, nodes, choices) {
    this.startId = id;
    this.nodes = nodes;
    this.choices = choices;
  }

  Step.prototype = {

    get id() {
      if (this.nodes.length > 1) {
        return "resultats";
      } else {
        return this.nodes[0].id;
      }
    },

    get title() {
      if (this.nodes.every(function(n) { return n.type === nodeTypes.INFO; })) {
        return "Résultats";
      } else {
        var title = [];
        this.nodes[0].expect.values.forEach(
          angular.bind(this, function(value) {
            if (this.choices[value.name])
              title.push(this.choices[value.name]);
          })
        );
        if (title.length > 0)
          return title.join(" ");
        else
          return this.nodes[0].title;
      }
    },

    get active() {
      return this === context.currentStep;
    }
  };

  // next step handling
  function nextStep(id, choices) {
    return graph.nextNodes(id, choices)
      .then(
        function(nodes) {
          // get choices for this step
          var newChoices = {};
          for (var n of nodes) {
            if (n.expect) {
              for (var v of n.expect.values) {
                newChoices[v.name] = choices[v.name]; 
              }
            }
          }
          return context.addStep(id, nodes, newChoices);
        },
        function(error) {
          return $q.reject(error);
        }
      );
  }
  function next(id, choices, stop) {
    nextStep(id, choices)
    .then(
      function(step) {
        if (!stop || step.startId === stop) {
          $location
            .path('/step/' + context.root + '/' + step.startId + '/')
            .search(context.choices);
          $rootScope.$emit(events.STEP_LOADED);
        }
        else if (stop) {
          next(step.id, choices, stop);
        }
      },
      function(error) {
        // just display the last correctly loaded step
        $rootScope.$emit(events.STEP_LOADED);
      }
    );
  }

  // back step handling
  function backStep(id) {
    var idx = null;
    for (var i = 0; i < context.steps.length; i++) {
      if (context.steps[i].startId == id) {
        idx = i;
        break;
      }
    }
    if (idx !== null) {
      var removed = context.steps.splice(idx + 1, context.steps.length);
      // refresh currentStep
      $rootScope.$emit(events.STEP_LOADED);
      context.currentStep.choices = {};
      $log.debug("Found step to go back to", context.currentStep);
    }
  }

  function goStep(root, id, choices) {
    if (context.startIds.indexOf(id) !== -1) {
      backStep(id);
    } else {
      if (id !== context.root && context.steps.length === 0) {
        next(context.root, choices, id);
      } else {
        next(id, choices);
      }
    }
  }
  
  $rootScope.$on(events.STEP_NEXT, function(evt, id, choices, root) {
    if (root)
      context.root = root;
    goStep(root, id, choices);
  });

  $rootScope.$on(events.STEP_LOADED, function() {
    context.currentStep = context.steps[context.steps.length - 1];
  });

  return {
    go: function(id, choices) {
      goStep(id, choices);
    },
    insertChoice: function(key, value) {
      context.currentStep.choices[key] = value;
      $log.debug("Choices are now", context.choices);
    },
    get root() {
      return context.root;
    },
    get steps() {
      return context.steps;
    },
    get choices() {
      return context.choices;
    },
    get currentStep() {
      return context.currentStep;
    },
    get currentStepExpect() {
      // return a list of params required for the current step
      // TODO: handle type ?
      return context.currentStep.nodes.reduce(function(acc, n) {
        n.expect.values.forEach(function(v) {
          acc.push(v.name);
        });
        return acc;
      }, []);
    }
  };

})

.factory('graph', function($http, $q, nodeTypes, config) {

  var graphURL = config.baseURL + '/graph/';

  function nextURL(id) {
    return graphURL + id + '/next';
  }

  function next(id, choices) {
    return $http({
      method: 'GET',
      url: nextURL(id),
      params: choices
    })
    .then(function(result) {
      return result.data.data;
    })
    .catch(function(result) {
      // throw errors and try
      // to recover in context
      if (result.data) {
        return $q.reject({
          status: result.status,
          errors: result.data.error
        });
      } else {
        return $q.reject({
          status: result.status,
          errors: [result.statusText]
        });
      }
    });
  }

  function nextNodes(id, choices, results) {
    if (typeof results === 'undefined') {
      // nodes will be acumulated in results
      // on first call results is undefined
      results = [];
    }

    return next(id, choices)
      .then(function(nodes) {
        // end of branch
        if (nodes.length === 0) {
          return results;
        }
        // got first level nodes
        nodes.forEach(function(n) {
          if (n.infos.length > 0)
            results.push(n);
        });
        // should we get next nodes ?
        if (nodes.every(nodeIsInfo)) {
          var r = [];
          nodes.forEach(function(n) {
            // recursively call nextNodes
            // passing current results
            r.push(nextNodes(n.id, choices, results));
          });
          // all callbacks finished
          // return results
          return $q.all(r).then(function() {
            return results;
          });
        } else {
          return results;
        }
      }
    );
  }

  function nodeIsInfo(n) {
    return n.type === nodeTypes.INFO;
  }

  function orderNodes(nodes) {
    return nodes.sort(function(a, b) {
      if (a.order == b.order)
        return 0;
      if (a.order > b.order)
        return 1;
      if (a.order < b.order)
        return -1;
    });
  }

  return {
    nextNodes: function(id, choices) {
      return nextNodes(id, choices).then(
        function(nodes) {
          return orderNodes(nodes);
        }
      );
    }
  };

})

.controller('breadcrumbCtrl', function($scope, $rootScope, $timeout, $httpParamSerializer, events, context) {
  var self = this;
  self.all = [];

  $scope.$watch(
    function() {
      return context.steps;
    },
    function(steps) {
      if (!steps)
        return;
      self.all = context.steps;
    }
  );

  self.stepUrl = function(step) {
    var choices = {};
    for (var s of self.all) {
      angular.extend(choices, s.choices);
      if (s.startId === step.startId)
        break;
    }
    return context.root + '/' + step.startId + '/?' + $httpParamSerializer(choices);
  };

  self.loading = false;

  $rootScope.$on(events.STEP_NEXT, function() {
    self.loading = true;
  });

  $rootScope.$on(events.STEP_LOADED, function() {
    $timeout(function() {
      self.loading = false;
    }, 200);
  });

  // TODO: style error
  $rootScope.$on(events.STEP_FAILED, function() {
    $timeout(function() {
      self.loading = false;
    }, 200);
  });

})

.controller('stepCtrl', function($rootScope, $scope, $routeParams, context, events) {
  // controller instanciated by the router
  // see $routeProvider config
  var self = this;
  self.current = null;

  var choices = {};
  for (var k in $routeParams) {
    if (k != 'id')
      choices[k] = $routeParams[k];
  }

  if (!context.currentStep || $routeParams.id !== context.currentStep.startId) {
    $rootScope.$emit(events.STEP_NEXT, $routeParams.id, choices, $routeParams.root);
  }

  // refresh the current step whenever
  // it changes
  $scope.$watch(
    function() {
      return context.currentStep;
    },
    function(current) {
      if (!current)
        return;
      self.current = current;
    }
  );

})

.directive('node', function() {

  return {
    templateUrl: 'partials/node.html',
    restrict: 'E',
    scope: {
      node: '<'
    },
  };

})

.directive('nodeInfo', function() {

  return {
    templateUrl: 'partials/node-info.html',
    restrict: 'E',
    scope: {
      node: '<'
    },
    controllerAs: 'nodeInfo',
    controller: function($scope) {
      var self = this;

      self.mapGeo = function() {
        return $scope.node.infos.reduce(function(geo, i) {
          if (i.geo)
            geo = i.geo;
          return geo;
        }, false);
      };

    }
  };

})

.directive('info', function() {

  return {
    template: '<div class="{{classes}}"><ng-include src="template" /></div>',
    restrict: 'E',
    scope: {
      info: '<'
    },
    link: function postLink(scope) {
      // based on the info type, choose the right template
      scope.template = 'partials/infos/'+scope.info.type+'.html';
      scope.classes = scope.info.type;
      if (scope.info.tags) {
        scope.classes = scope.info.tags.reduce(function(acc, tag) {
          tag = tag.split(':');
          if (tag[0] === 'css')
            acc.push(tag[1]);
          return acc;
        }, [scope.info.type]).join(" ");
      }
    }
  };

})

.directive('customLink', function () {

  function link($scope, element, attr) {
    var a, regex, text, pre, post, tmp;

    regex = /(.*)\[(.+)\](.*)/;
    text  = (document.body.innerText) ? 'innerText' : 'textContent';
    pre   = '';
    post  = '';
    a     = '<a href="'+ attr.url +'"';
    if (attr.blank) { a += ' target="_blank"'; }
    if (regex.test(attr.content)) {
      tmp   = attr.content.match(regex);
      pre   = tmp[1];
      post  = tmp[3];
      a += '>' + tmp[2] + '</a>';
      attr.content = attr.content.replace(regex, a);
    } else {
      a += '>' + attr.content + '</a>';
    }

    element.html(pre + a + post);
  }

  return {
    restrict: 'E',
    scope: {
      info: '<'
    },
    link: link
  };

})

.controller('bottomCtrl', function($location, $scope, context) {
  var self = this;

  $scope.$watch(
    function() {
      return context.currentStep;
    },
    function(newVal) {
      // FIXME
      if (newVal && newVal.title === "Résultats") {
        self.url = $location.absUrl();
      } else {
        self.url = null;
      }
    }
  );

})

.controller('radioCtrl', function($scope, $rootScope, events, context) {
  // controller for radio choices
  var self = this;

  self.choice = null;
  self.error = null;

  self.next = function() {
    if (self.choice && self.choice instanceof Array) {
      $rootScope.$emit(events.STEP_NEXT, context.currentStep.id, context.choices);
    }
    else
      self.error = "Vous devez faire un choix";
  };

  $scope.$watch(
    function() {
      return self.choice;
    },
    function(newVal) {
      if (newVal && newVal instanceof Array) {
        // on radio selection insert the choice
        // in the context
        context.insertChoice(newVal[0], newVal[1]);
        self.error = null;
      }
    }
  );

})

.controller('inputCtrl', function($log, $http, $scope, $rootScope, config, events, context) {
  // controller for input choices
  var self = this;
  var expect = context.currentStepExpect;

  self.info = $scope.info;
  self.choice = null;
  self.selected = false;
  self.error = null;

  self.next = function() {
    if (self.selected) {
      $rootScope.$emit(events.STEP_NEXT, context.currentStep.id, context.choices);
    }
    else if (self.choice)
      self.error = "Vous devez sélectionner une ville dans la liste des suggestions";
    else
      self.error = "Aucune ville selectionnée";
  };

  self.autocompleteURL = function() {
    if (self.info.api.path) {
      var path = self.info.api.path.join('/');
      return config.baseURL + self.info.api.base_url + '/' + path;
    } else {
      return config.baseURL + self.info.api.base_url;
    }
  };

  self.autocompleteData = [];

  self.select = function(item) {
    var choice = [];
    for (var k in item) {
      if (expect.indexOf(k) !== -1) {
        choice.push(item[k]);
        context.insertChoice(k, item[k]);
      }
    }
    self.choice = choice.join(" ");
    self.error = null;
    self.selected = true;
  };

  self.autocomplete = function(term) {
    if (!term || term.length < 3) {
      return;
    }

    // prepare request
    var params = {};
    if (self.info.api.params.length > 1) {
      $log.error("More that one params for autocomplete", self.info.api.params);
      return;
    } else {
      params[self.info.api.params[0]] = term;
    }

    $http({
      method: 'GET',
      url: self.autocompleteURL(),
      params: params
    }).then(
      function(result) {
        self.autocompleteData = [];
        for (var i = 0; i < 20; i++) {
          var data = {},
              o = result.data.data[i];
          for (var k in o) {
            if (expect.indexOf(k) !== -1) {
              data[k] = o[k];
            }
          }
          if (Object.keys(data).length > 0) {
            self.autocompleteData.push(data);
          }
        }
      }
    );
  };

})

.controller('accordionCtrl', function() {
  var self = this;

  this.state = {};
  this.toggle = function(index) {
    self.state[index] = !self.state[index];
  }
  this.isVisible = function(index) {
    return self.state[index] == true;
  }

})

.filter('toLines', function($sce) {
    return function(input) {
      if (Array.isArray(input) && input.length > 0) {
        return $sce.trustAsHtml(input.join('<br/>'));
      } else if (typeof input === 'string' || input instanceof String) {
        input = input.split(/\r?\n/);
        return $sce.trustAsHtml(input.join('<br/>'));
      }
      return '';
    };
  }
)

.directive('map', function($timeout) {

  return {
    template: '<div class="map"></div>',
    restrict: 'E',
    scope: {
      geo: '&'
    },
    link: function(scope, element, attrs) {
      // unwrap method
      scope.geo = scope.geo();
      scope.$watch('geo', function(geo) {
        showMap(element.find('div')[0], geo());
      });

      function showMap(elem, geo, name) {
        var marker = new ol.Feature({
          geometry: new ol.geom.Point(
            ol.proj.transform(geo, 'EPSG:4326', 'EPSG:3857')
          ),
          name: name
        });
        var vectorSource = new ol.source.Vector({
          features: [marker]
        });
        var iconStyle = new ol.style.Style({
          image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 1,
            src: 'img/map-marker-icon.png'
          }))
        });
        var vectorLayer = new ol.layer.Vector({
          source: vectorSource,
          style: iconStyle
        });
        var map = new ol.Map({
          target: elem,
          layers: [
            new ol.layer.Tile({
              source: new ol.source.OSM()
            }),
            vectorLayer
          ],
          view: new ol.View({
            center: ol.proj.fromLonLat(geo),
            zoom: 17
          })
        });

        // workaround to have a correct
        // width on the map
        $timeout(function() {
          map.updateSize();
        }, 300);
      }
    }
  };

})

.directive('stepButton', function() {

  return {
    restrict: 'E',
    templateUrl: 'partials/step-button.html',
    controllerAs: 'stepButton',
    controller: function($rootScope, events) {
      var self = this;

      self.loading = false;

      $rootScope.$on(events.STEP_NEXT, function() {
        self.loading = true;
      });

      $rootScope.$on(events.STEP_LOADED, function() {
        self.loading = false;
      });

      $rootScope.$on(events.STEP_FAILED, function() {
        // TODO: error style and such
        self.loading = false;
      });
    }
  };

})

.directive('top', function() {

  return {
    restrict: 'E',
    template: '<div ng-show="top.show" class="up" ng-click="top.scroll()"><i class="fa fa-chevron-up"></i></div>',
    controllerAs: 'top',
    controller: function($scope, $anchorScroll, $window) {
      var self = this;

      this.scroll = function() {
        $anchorScroll();
      };

      $window.onscroll = function() {
        var top = document.body.scrollTop || document.documentElement.scrollTop || 0,
            height = document.body.scrollHeight || document.documentElement.scrollHeight || 0;
        if (top > height / 20) {
          self.show = true;
        } else {
          self.show = false;
        }
        $scope.$applyAsync();
      };
    },
  };

});
